use model_data::OsdiModelStruct;
use proc_macro::TokenStream;
use quote::ToTokens;
use syn::parse_macro_input;

mod model_data;

#[proc_macro_attribute]
pub fn osdi_model_data(_attr: TokenStream, input: TokenStream) -> TokenStream {
    let data = parse_macro_input!(input as OsdiModelStruct);
    let res = data.into_token_stream();
    TokenStream::from(res)
}
