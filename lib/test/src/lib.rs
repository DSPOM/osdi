use osdi::runtime::abi::ModelCard;
use osdi::runtime::{ModelId, ModelSizedType};
use osdi_derive::osdi_model_data;

#[osdi_model_data]
pub struct Test {
    x: u8,
    y: [(u32, u8); 31],
    z: (u8, u16),
    lel: ModelId,

    #[model_data]
    modelcard: ModelCard,
    #[model_data]
    modelcard2: ModelCard,
}

unsafe impl ModelSizedType for TestHead {
    fn model(&self) -> ModelId {
        self.lel
    }
}
