{ pkgs }:

with pkgs;
{
  buildInputs = let
    mkStatic = stdenv.lib.flip stdenv.lib.overrideDerivation (
      o: {
        dontDisableStatic = true;
        configureFlags = stdenv.lib.toList (o.configureFlags or []) ++ [ "--enable-static" ];
        buildInputs = map mkStatic (o.buildInputs or []);
        propagatedBuildInputs = map mkStatic (o.propagatedBuildInputs or []);
      }
    );
  in
    map mkStatic [ libffi ncurses ] ++ [ llvmPackages_latest.llvm libxml2 zlib.static ];

  nativeBuildInputs = [
    gcc
    cmake
    llvmPackages_latest.clang
    llvmPackages_latest.libclang
  ];

  LLVM_SYS_110_PREFIX = "${llvmPackages_latest.llvm}";
  LIBCLANG_PATH = "${llvmPackages_latest.libclang}/lib";
  NGSPICE_SRC = builtins.fetchGit {
    url = "git://git.code.sf.net/p/ngspice/ngspice";
    rev = "279edff5d9877b13ddd7708285553ea20d6f14cd";
  };
}
