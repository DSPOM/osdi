//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::errors::{NgspiceError, SetupError};
use crate::ffi;
use crate::ffi::{CKTcircuit, GENinstance, SMPmatrix};
use crate::modelcard::ModelCard;
use osdi::model_info_store::{PortId, LimitId};
use osdi::runtime::abi::{BranchCurrentsAndConductance, InstanceVariables, JacobiPtrs, NodePotentialOffsets};
use osdi::runtime::{ModelData, ModelId, ModelSizedType, OsdiRegistry};
use osdi::{ModelInfoStore, OsdiModel, ReturnFlags, LoadFlags};
use osdi_derive::osdi_model_data;
use static_assertions::_core::convert::Infallible;
use std::convert::{TryFrom, TryInto};
use std::ffi::c_void;
use std::ops::Deref;
use std::os::raw::c_int;
use std::ptr::NonNull;
use tracing::debug;
use static_assertions::_core::option::Option::Some;
use index_vec::{IndexSlice};

/// Compile time asserts to ensure that memory layout is as ngspice expects (this has no effect on runtime expect making sure it works)
mod assert {
    #![doc(hidden)]
    #![allow(warnings)]
    use super::*;
    use static_assertions::{assert_eq_align, assert_eq_size};
    use std::os::raw::c_int;

    #[repr(C)]
    struct A {
        gen: GENinstance,
        data: [u64; 2],
    }

    #[repr(C)]
    struct B {
        gen: GENinstance,
        data1: c_int,
        data2: c_int,
        data3: c_int,
        data4: c_int,
    }
    #[repr(C)]
    struct C {
        gen: GENinstance,
        data: [c_int; 4],
    }
    assert_eq_align!(A, B, C, GENinstance);
    assert_eq_size!(A, B, C);
}

#[repr(transparent)]
pub struct TerminalPotentialOffsets([c_int]);

impl TerminalPotentialOffsets {
    pub fn get_offset(&self, node: PortId) -> c_int {
        self.0[node.index()]
    }
}

impl ModelData for TerminalPotentialOffsets {
    fn size(model: &ModelInfoStore) -> usize {
        (model.ports.len() + 2) / 2
    }

    unsafe fn coerce(raw: &mut [u64]) -> &mut Self {
        &mut *(raw as *mut [u64] as *mut Self)
    }
}

#[osdi_model_data]
pub struct OsdiInstance {
    /// NGSPICE has a general instance struct.
    /// It is expected to be at the start of each instance struct so that
    /// * mut GENinstance can be cast to *mut OsdiInstace (and back)
    pub general_instance: GENinstance,

    /// NGSPICE requires that each struct has an array of integers that the ptr offsets of these nodes is written to
    /// Since OSDI requries that all nodes are in a continuous slice of memory this data will later be copied to `voltage_offset`
    #[model_data]
    pub terminal_voltage_offset: TerminalPotentialOffsets,

    /// store offsets from which pointers to node voltages can be calculated from a base PTR
    #[model_data]
    pub voltage_offset: NodePotentialOffsets,

    /// Stores branch voltages and their (non zero) derivatives
    #[model_data]
    pub flows_and_conductance: BranchCurrentsAndConductance,

    /// Stores branch voltages and their (non zero) derivatives
    #[model_data]
    pub matrix_ptrs: JacobiPtrs,

    /// Stores branch voltages and their (non zero) derivatives
    #[model_data]
    pub variables: InstanceVariables,
}

pub struct InstanceIter<'a>(Option<NonNull<GENinstance>>, &'a OsdiRegistry);

impl<'a> InstanceIter<'a> {
    pub unsafe fn from_ffi(ptr: *mut GENinstance, regirsty: &'a OsdiRegistry) -> Self {
        Self(NonNull::new(ptr), regirsty)
    }
}

impl<'a> Iterator for InstanceIter<'a> {
    type Item = OsdiInstance<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let instance = unsafe { OsdiInstance::from_ffi(self.0?.as_ptr(), self.1) };
        self.0 = NonNull::new(instance.general_instance.GENnextInstance);
        Some(instance)
    }
}

// Instance head is generated by the macro.
// Its the OsdiInstance without the model data fields
// this head struct is then used to determine the model and based upon that the actual size of the struct at runtime
unsafe impl ModelSizedType for OsdiInstanceHead {
    fn model(&self) -> ModelId {
        self.general_instance.model_id()
    }
}
impl GENinstance {
    pub fn model_id(&self) -> ModelId {
        unsafe { &*self.GENmodPtr }.model_id()
    }
}

impl OsdiInstance<'_> {

    pub unsafe fn from_ffi(gen: *mut GENinstance, registry: &OsdiRegistry) -> Self {
        Self::from_ptr_with_registry(gen as *mut OsdiInstanceData, registry)
    }

    pub fn state_vec_ptr(&self,ckt: &CKTcircuit, time_step: usize) -> *mut f64 {
        unsafe { self.calc_state_vec_ptr(ckt.CKTstates[time_step]) }
    }

    pub unsafe fn calc_state_vec_ptr(&self, state_vec: *mut f64) -> *mut f64 {
        state_vec.add(self.general_instance.GENstate as usize)
    }


    pub fn non_linearities(
        &self,
        model: &OsdiModel,
        ckt: &CKTcircuit,
        time_step: usize,
    ) -> &IndexSlice<LimitId,[f64]> {
        debug_assert_eq!(model.id, self.model());

        unsafe {
            let ptr = self.calc_state_vec_ptr(ckt.CKTstates[time_step]);
            IndexSlice::from_raw_parts(ptr, model.info_store.non_linear_voltages.len())
        }
    }

    fn non_linearities_mut(
        &mut self,
        model: &OsdiModel,
        ckt: &CKTcircuit,
    ) -> &mut IndexSlice<LimitId,[f64]> {
        debug_assert_eq!(model.id, self.model());

        unsafe {
            let ptr = self.calc_state_vec_ptr(ckt.CKTstates[0]);
            IndexSlice::from_raw_parts_mut(ptr, model.info_store.non_linear_voltages.len())
        }
    }

    pub fn load_non_linearaties_rom_old_rhs(&mut self, ckt: &CKTcircuit, model: &OsdiModel){
        debug_assert_eq!(model.id, self.model());

        let dst: &mut IndexSlice<LimitId,_> = unsafe {
            let ptr = self.calc_state_vec_ptr(ckt.CKTstates[0]);
            IndexSlice::from_raw_parts_mut(ptr, model.info_store.non_linear_voltages.len())
        }; // Cant use self.non_linearties_mut() because then the borrow isnt splot properly (but its perfectly fine to write to the statevec and read from any data of self

        for (dst,&non_linearity) in dst.iter_mut().zip(model.info_store.non_linear_voltages.iter()){
            *dst = unsafe { self.voltage_offset.get_pot(ckt.CKTrhsOld, non_linearity) }
        }
    }


    /// This function calls the compiled load_routine compiled for this moddel to calculate all currents and derivatives required for the [`load_into_matrix`] function.
    /// # Returns
    /// The [status flags](osdi::ReturnFlags) that the load_function returned (such as termination hints, or limit indicators)
    pub unsafe fn load_routine(
        &mut self,
        model: &OsdiModel,
        mc: &ModelCard,
        ckt: &CKTcircuit,
        flags: LoadFlags
    ) -> ReturnFlags {
        // Calculate the state_vec by adding the instance offset (GENstate) to the start of the state vec of each time step (ckt.CKTstates)
        let mut state_vecs: [*mut f64; 8] = ckt.CKTstates;

        for state_vec in state_vecs.iter_mut() {
            *state_vec = self.calc_state_vec_ptr(*state_vec)
        }

        let stat_vec_ptr = state_vecs.as_mut_ptr() as *mut *mut u64;

        // Compiled code calculates all currents and automatically generated derivatives as specified in the Verilog Code

        model.load_routine(
            ckt as *const CKTcircuit as *mut c_void,
            &mc.parameters,
            &mc.variables,
            self.variables,
            stat_vec_ptr,
            ckt.CKTrhsOld, // assuming a predictor is used voltages can always be read from the old Rhs
            self.voltage_offset,
            self.flows_and_conductance,
            flags
        )
    }

    // TODO MARKUS REVIEW
    /// This function is called during the load routine after the shared libary function completes (so all currents, and derivatives have been computed).
    /// Its purpose is to calculate and write all required values into the ngspice solver.
    /// NGSPICE solves the NEWTON iteration with the following iteration rule (used for better numerical stability)
    /// ```math
    ///     J*V'=-I+J*V
    /// ```
    /// `J` is the (sprase) Jacobian matrix where each element is given by the following:
    /// ```math
    ///     J_{ij} = \Sum_j \frac{dI_{i}}{dV_j}
    /// ```
    /// `I_i`is the Kirchhoff current conservation law for the Node `i` (solved for 0 however during the iteration the result is non zero)
    /// ```math
    ///     I_i = \Sum_j I_{ij}
    /// ```
    /// `V_j` is the Potential of another Node `j`.
    /// Using similar notation the right hand side vector of the iteration rule can be written as
    ///
    /// ```math
    ///     rhs_i = -I_i + \Sum_k \frac{dI_i}{dV_k}*V_k = -I_i + \Sum_j \Sum_k \frac{dI_{ij}}{dV_k}*V_k
    /// ```
    ///
    /// All (non zero) derivatives and currents per branch were previously calculated by the compiled modelcode.
    /// Therefore the above equations are calculated and written to the appropriate pointers and this functions.
    pub fn load_into_matrix(&self, model: &OsdiModel, ckt: &CKTcircuit) {
        let non_linear_voltages = self.non_linearities(model, ckt, 0);


        // TODO flatten the loop for improved performance?
        for branch in model.info_store.branches.iter() {
            // I_{ij}
            let current = self.flows_and_conductance.get_current(branch);

            // \Sum_k dI_ij / dV_k * V_k
            let mut jacobian_row_times_voltages = 0.0;

            for (stamp, /* dI_ij / dV_k */ derivatve_val) in self.flows_and_conductance.stamps(branch) {
                unsafe { self.matrix_ptrs.stamp(stamp, derivatve_val) } // write into SparseMatrix

                // V_k
                // TODO we neglect that limited voltages can affect other branches as well here
                // TODO check if that makes any difference in practice (if yes come up with a way to figure this out at compile time)
                let derivative_pot = match stamp.voltage.lim{
                    Some(lim) => non_linear_voltages[lim],
                    None => unsafe{self.voltage_offset.get_pot(ckt.CKTrhsOld, stamp.voltage)}
                };

                // dI_ij / dV_k * V_k
                jacobian_row_times_voltages += derivative_pot * derivatve_val
            }

            // Branches are only saved once in the info store
            // Which node is the high node and which one is the low node is arbitrary
            // Therefore outgoing nodes need to also be subtracted (sign is changed)
            // Therefore the result for each branch needs to be added to the kirchoff law of both its hi and lo node.
            // In the latter case the sign needs to be sweapped as follows:
            //
            // \Sum_k dI_ij / dV_k * V_k - I_ij (hi node)
            // -(\Sum_k dI_ij / dV_k * V_k - I_ij (lo node)
            //
            // Additionally currents are not added to ground nodes as the kirchoff law is always satisfied there (all currents are compensated)
            // Therefore the contribution to the low and high node rhs entry needs to be calcualted seperately

            // TODO MARKUS REVIEW stimmt das so? Bin mir nicht sicher ob rhs für ground immer 0 ist oder nur die ströme nicht addiert werden

            let hi_rhs = if model.info_store.nodes[branch.voltage.hi].ground {
                jacobian_row_times_voltages
            } else {
                jacobian_row_times_voltages - current
            };

            let lo_rhs = if model.info_store.nodes[branch.voltage.lo].ground {
                -jacobian_row_times_voltages
            } else {
                current - jacobian_row_times_voltages
            };

            unsafe {
                self.voltage_offset.increment(ckt.CKTrhs, branch.voltage.hi, hi_rhs);
                self.voltage_offset.increment(ckt.CKTrhs, branch.voltage.lo, lo_rhs);
            }
        }
    }


    // TODO MARKUS REVIEW
    /// This function performs the setup required for each instance:
    ///
    /// * Create all required nodes (that are not terminals)
    /// * Collapse nodes which don't need to be created (TODO)
    /// * Write the indecies of all created nodes to the instance
    /// * Create all required Jacobian entries
    /// * Write all jacobian pointers to the instance
    /// * Reserve space for time derivatives and limt functions in the state vector
    /// * Save the state vector position of this instance
    pub fn setup(
        &mut self,
        model: &OsdiModel,
        mc: &ModelCard,
        matrix: *mut SMPmatrix,
        ckt: &mut CKTcircuit,
        state_vec_offset: &mut c_int,
    ) -> Result<(), SetupError> {
        // TODO is this actually required? No instance variables should be computable at this point
        // TODO handle flags
        unsafe {
            self.variables.init(&mc.parameters, model);
        }
        // Reserve space for time derivative and limit values in the space vectors
        self.general_instance.GENstate = *state_vec_offset;
        *state_vec_offset += i32::try_from(model.info_store.non_linear_voltages.len()+2*model.info_store.ddt_count).unwrap();
        // state vec layout:
        // TODO collapse nodes

        for (id, node) in model.info_store.nodes.iter_enumerated() {
            if let Some(port) = node.port {
                if model.info_store.ports[port].net == id {
                    // This is a terminal just copy the offset
                    self.voltage_offset.set_offset(
                        id,
                        self.terminal_voltage_offset
                            .get_offset(port)
                            .try_into()
                            .unwrap(),
                    );
                    continue;
                }
            };

            // Create node
            let node_ptr = ckt.make_node(self.general_instance.GENname, node.name)?;
            self.voltage_offset
                .set_offset(id, node_ptr.number.try_into().unwrap());

            // Copy nodeset from associated port
            // TODO How do we get this info from Verilog?
            if let Some(port) = node.port {
                ckt.copy_nodesets(
                    &self.general_instance,
                    port.index().try_into().unwrap(),
                    node_ptr,
                )
            }
        }

        // create matrix elements
        for (id, entry) in model.info_store.jacobian.iter_enumerated() {
            // each Jacobin entry represents the kirchhoff current law (sum of al incoming currents) derived by another voltage

            // row is the node to which the currents belong
            let kirchoff_node = self.voltage_offset.get_offset(entry.kirchoff_node);
            // column is the node the derivatives was taken by
            let derive_by_node = self.voltage_offset.get_offset(entry.derived_by);

            let res = unsafe {
                ffi::SMPmakeElt(
                    matrix,
                    kirchoff_node.try_into().unwrap(),
                    derive_by_node.try_into().unwrap(),
                )
            };

            // null pointer is returned if out of memory
            if res.is_null() {
                return Err(NgspiceError::OUT_OF_MEMORY.into());
            } else {
                self.matrix_ptrs.set_ptr(id, res)
            }
        }
        Ok(())
    }

    pub fn unsetup(&mut self, model: &OsdiModel, ckt: &mut CKTcircuit) -> Result<(), NgspiceError> {
        for (id, node) in model.info_store.nodes.iter_enumerated() {
            // TODO ignore collapsed nodes
            match node.port {
                Some(port) if model.info_store.ports[port].net == id => (), // Terminals are deleted by ngspice,
                _ => ckt.delete_node(self.voltage_offset.get_offset(id) as i32)?,
            }

            self.voltage_offset.set_offset(id, 0)
        }
        Ok(())
    }

    pub fn has_converged(&self, model: &OsdiModel, ckt: &CKTcircuit) -> Result<bool, Infallible> {
        let non_linear_voltages = self.non_linearities(model, ckt, 0);

        for branch in model.info_store.branches.iter() {
            let current = self.flows_and_conductance.get_current(branch);
            let delta: f64 = self.flows_and_conductance.non_linearity_sensitivities(branch)
                .map(|(&lim,sensitivity)| {
                    let voltage = non_linear_voltages[lim];
                    voltage*sensitivity
                })
                .sum();


            let rel_tol = ckt.CKTreltol * f64::max((current + delta).abs(), current.abs());
            if rel_tol + ckt.CKTabstol > delta.abs() {
                debug!(
                    model = model.info_store.name.deref(),
                    branch = branch.name.deref(),
                    delta = display(delta),
                    "OSDI: Branch didn't converge due to non-linearites"
                );
                return Ok(false);
            }
        }
        Ok(true)
    }
}
