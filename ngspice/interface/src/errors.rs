//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::ffi;
use osdi::model_info_store::ParameterId;
use osdi::runtime::{ModelId, OsdiRegistry};
use std::convert::Infallible;
use std::error::Error;
use std::ffi::CStr;
use std::fmt::{Display, Formatter};
use std::num::NonZeroI32;
use std::ops::Deref;
use std::os::raw::{c_char, c_int};
use thiserror::Error;

impl FfiError for Infallible {
    fn status_code(&self) -> i32 {
        match *self {}
    }

    fn just_status_code(&self) -> bool {
        match *self {}
    }
}
pub trait FfiError {
    fn status_code(&self) -> c_int;
    fn just_status_code(&self) -> bool;
}

#[derive(Debug, Clone, PartialEq)]
pub struct OutOfBoundsParameters {
    pub parameters: Vec<ParameterId>,
    pub model_name: *const c_char,
    pub model: ModelId,
}

impl Display for OutOfBoundsParameters {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let name = unsafe { CStr::from_ptr(self.model_name) }.to_string_lossy();
        f.write_fmt(format_args!(
            "The model {} is invalid! The following parameters are out of bounds: ",
            name.deref()
        ))?;
        OsdiRegistry::with_model(self.model, |model| {
            let mut iter = self.parameters.iter();
            if let Some(first) = iter.next() {
                f.write_str(model.info_store.parameters[*first].info.name.deref())?;
                for param in iter {
                    f.write_str(", ")?;
                    f.write_str(model.info_store.parameters[*param].info.name.deref())?;
                }
            }
            Ok(())
        })
    }
}

unsafe impl Send for OutOfBoundsParameters {}
unsafe impl Sync for OutOfBoundsParameters {}
impl Error for OutOfBoundsParameters {}

#[derive(Debug, Clone, Error)]
pub enum SetupError {
    #[error("{0}")]
    OutOfBounds(#[from] OutOfBoundsParameters),
    #[error("{0}")]
    NgspiceError(#[from] NgspiceError),
}

impl FfiError for SetupError {
    fn status_code(&self) -> i32 {
        match self {
            Self::NgspiceError(err) => err.status_code(),
            Self::OutOfBounds(_) => ffi::E_PARMVAL,
        }
    }

    fn just_status_code(&self) -> bool {
        matches!(self, Self::NgspiceError(_))
    }
}

#[derive(Debug, Copy, Clone)]
pub struct NgspiceError(NonZeroI32);

impl NgspiceError {
    pub const OUT_OF_MEMORY: Self = Self(unsafe { NonZeroI32::new_unchecked(ffi::E_NOMEM) });
}

impl FfiError for NgspiceError {
    fn status_code(&self) -> i32 {
        0
    }

    fn just_status_code(&self) -> bool {
        true
    }
}

impl Display for NgspiceError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Error occurred inside ngspice. Status code {}",
            self.0
        ))
    }
}

impl Error for NgspiceError {}

#[doc(hidden)]
mod _sealed {
    use crate::errors::NgspiceError;
    #[doc(hidden)]
    pub trait Sealed {}
    impl Sealed for Result<(), NgspiceError> {}
}

pub trait FromFfi: _sealed::Sealed + Sized {
    fn from_status_code(code: c_int) -> Self;
}

impl FromFfi for Result<(), NgspiceError> {
    fn from_status_code(code: i32) -> Self {
        if let Some(err) = NonZeroI32::new(code) {
            Err(NgspiceError(err))
        } else {
            Ok(())
        }
    }
}
