//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::errors::{NgspiceError, SetupError};
use crate::ffi::{CKTcircuit, SMPmatrix};
use crate::modelcard::ModelIter;
use osdi::runtime::OsdiRegistry;
use osdi::{OsdiModel, ReturnFlags, LoadFlags};
use std::convert::Infallible;
use std::os::raw::c_int;
use tracing::debug;
// use crate::runtime::TemporaryStorage;

mod device;
mod errors;
mod ffi;
mod instance;
mod modelcard;
// mod runtime;

pub const OSDI_DEVICE_OFFSET: usize = 0;

fn setup(
    regirsty: &OsdiRegistry,
    model: &OsdiModel,
    matrix: *mut SMPmatrix,
    ckt: &mut CKTcircuit,
    state_vec_offset: &mut c_int,
    modelcards: ModelIter,
) -> Result<(), SetupError> {
    for mut mc in modelcards {
        mc.setup(regirsty, model, matrix, ckt, state_vec_offset)?
    }
    Ok(())
}

fn unsetup(
    registry: &OsdiRegistry,
    model: &OsdiModel,
    ckt: &mut CKTcircuit,
    mcards: ModelIter,
) -> Result<(), NgspiceError> {
    for mc in mcards {
        for mut instance in unsafe { mc.instances(registry) } {
            instance.unsetup(model, ckt)?
        }
    }
    Ok(())
}


fn load(
    registry: &OsdiRegistry,
    model: &OsdiModel,
    ckt: &CKTcircuit,
    modelcards: ModelIter,
) -> Result<(), Infallible> {
    let flags = LoadFlags::EMPTY;

    for mc in modelcards {
        for mut instance in unsafe { mc.instances(registry) } {
            // TODO multithread this with a seperate rayon iter
            // TODO move match out of loop?

            // todo handle flags
            let flags = match ckt.CKTmode & ffi::ININIT_MODE_MASK {
                ffi::MODEINITSMSIG => {
                        unsafe {
                            instance.load_routine(
                                model,
                                &mc,
                                ckt,
                                flags
                            )
                        }
                    },

                ffi::MODEINITTRAN =>  {
                    unsafe {
                        instance.load_routine(
                            model,
                            &mc,
                            ckt,
                            flags
                        )
                    }
                },

                // TODO inital guess
                // TODO init to something reasoable
                // TODO handle off

                _ => {
                    if ckt.CKTbypass != 0 && instance.has_converged(model, ckt)? {
                        // TODO don't skip for selfheating???
                        debug!("Model already converged! Bypassing load!");
                        unsafe { ReturnFlags::from_bits_unchecked(0) }
                    } else {
                        instance.load_non_linearaties_rom_old_rhs(ckt,model);
                        unsafe {
                            instance.load_routine(
                                model,
                                &mc,
                                ckt,
                                flags
                            )
                        }
                    }
                }
            };

            if ckt.CKTmode & ffi::MODEINITSMSIG == 0 {
                instance.load_into_matrix(model, ckt)
            } else {
                // TODO compute additonal small signal parameters and save to instance
            }
        }
    }

    Ok(())
}

fn check_convergens(
    registry: &OsdiRegistry,
    model: &OsdiModel,
    ckt: &mut CKTcircuit,
    modelcards: ModelIter,
) -> Result<(), Infallible> {
    for mc in modelcards {
        for instance in unsafe { mc.instances(registry) } {
            if !instance.has_converged(model, ckt)? {
                ckt.CKTnoncon += 1;
                ckt.CKTtroubleElt =
                    &instance.general_instance as *const ffi::GENinstance as *mut ffi::GENinstance;
                return Ok(());
            }
        }
    }
    Ok(())
}
