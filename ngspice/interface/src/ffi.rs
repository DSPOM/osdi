//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::errors::{FfiError, FromFfi, NgspiceError};
use crate::modelcard::{ModelCard, ModelIter};
use core::fmt::Display;
use osdi::model_info_store::{ParameterId, UserInfo};
use osdi::runtime::OsdiRegistry;
use osdi::types::{SimpleType, TypeInfo};
use osdi::{OsdiModel, OsdiStr};
use std::convert::TryInto;
use std::ffi::CString;
use std::fmt::Debug;
use std::ops::Deref;
use std::os::raw::{c_char, c_int, c_long, c_void};
use std::panic::UnwindSafe;

#[allow(warnings)]
mod bindings {
    include!(concat!(env!("OUT_DIR"), "/devdefs.rs"));
}

// Errors numbers
pub const E_NOMEM: c_int = bindings::E_NOMEM as c_int;
pub const E_PANIC: c_int = bindings::E_PANIC as c_int;
pub const E_PARMVAL: c_int = bindings::E_PARMVAL as c_int;
pub const E_PRIVATE: c_int = bindings::E_PRIVATE as c_int;

pub const MODEINITFLOAT: c_long = bindings::MODEINITFLOAT as c_long;
pub const MODEINITJCT: c_long = bindings::MODEINITJCT as c_long;
pub const MODEINITFIX: c_long = bindings::MODEINITFIX as c_long;

pub const MODEINITSMSIG: c_long = bindings::MODEINITSMSIG as c_long;
pub const MODEINITTRAN: c_long = bindings::MODEINITTRAN as c_long;
pub const MODEINITPRED: c_long = bindings::MODEINITPRED as c_long;

pub const ININIT_MODE_MASK: c_long = MODEINITFLOAT
    | MODEINITJCT
    | MODEINITFIX
    | MODEINITSMSIG
    | MODEINITTRAN
    | MODEINITPRED as c_long;



pub const MODE_MASK: c_long = bindings::MODE as c_long;
pub const MODEAC: c_long = bindings::MODEAC as c_long;
pub const MODETRAN: c_long = bindings::MODETRAN as c_long;


pub use bindings::*;

macro_rules! const_cstr {
    ($content:literal) => {
        concat!($content, "\0").as_ptr() as *const c_char
    };

    ($name:ident = $content:literal) => {
        const $name: *const c_char = const_cstr!($content);
    };
}

// Functions to register osdi with ngspice

#[no_mangle]
pub unsafe extern "C" fn register_osdi_devs(devices: *mut SPICEdev) {
    let res = std::panic::catch_unwind(|| {
        OsdiRegistry::with(|registry| {
            for (off, model) in registry.models().enumerate() {
                std::ptr::write(devices.add(off), SPICEdev::from_osdi(model));
            }
        })
    });

    if res.is_err() {
        println!(
            "OSDI: Panic occurred during device registration (register_osdi_devs)! ABORTING..."
        );
        std::process::abort()
    }
}

#[no_mangle]
pub unsafe extern "C" fn registered_osdi_dev_count() -> usize {
    let res = std::panic::catch_unwind(|| OsdiRegistry::with(|registry| registry.model_count()));

    match res {
        Ok(res) => res,
        Err(_) => {
            println!("OSDI: Panic occurred during device registration (registered_osdi_dev_count)! ABORTING...");
            std::process::abort()
        }
    }
}

fn ffi_wrapper(
    f: impl FnOnce(&OsdiRegistry) -> c_int + UnwindSafe,
    routine_name: *const c_char,
) -> c_int {
    match std::panic::catch_unwind(|| OsdiRegistry::with(|registry| f(registry))) {
        Ok(res) => res,
        Err(_) => {
            // Panic message was automatically printed by panic handler
            if let Ok(res) = CString::new("OSDI: Panic occurred during model access") {
                let err = Box::leak(res.into_boxed_c_str());
                unsafe {
                    errMsg = err.as_ptr() as *mut c_char;
                    errRtn = routine_name as *mut c_char;
                }
            }
            E_PANIC
        }
    }
}

fn err_handler<E: Display + FfiError + Debug>(
    routine: impl FnOnce() -> Result<(), E> + UnwindSafe,
    model: &OsdiModel,
    routine_name: *const c_char,
) -> c_int {
    match std::panic::catch_unwind(routine) {
        Ok(Ok(_)) => 0,
        Ok(Err(err)) if err.just_status_code() => err.status_code(),

        Ok(Err(err)) => {
            println!("{}", err);
            if let Ok(res) = CString::new(format!(
                "OSDI: {:?} error occurred in {} model",
                err,
                model.info_store.name.deref()
            )) {
                let err = Box::leak(res.into_boxed_c_str());
                unsafe {
                    errMsg = err.as_ptr() as *mut c_char;
                    errRtn = routine_name as *mut c_char;
                }
            }
            err.status_code()
        }
        Err(_) => {
            // Panic message was automatically printed by panic handler
            if let Ok(res) = CString::new(format!(
                "OSDI: Panic occurred in {} model",
                model.info_store.name.deref()
            )) {
                let err = Box::leak(res.into_boxed_c_str());
                unsafe {
                    errMsg = err.as_ptr() as *mut c_char;
                    errRtn = routine_name as *mut c_char;
                }
            }
            E_PANIC
        }
    }
}

const_cstr!(LOAD_ROUTINE_NAME = "load");

/// Load function is called by ngspice on each iteration
/// Its the "main" method of the simulation. It calculates all branch currents and derivatives
pub unsafe extern "C" fn load(gen_model: *mut GENmodel, ckt: *mut CKTcircuit) -> c_int {
    ffi_wrapper(
        |registry| {
            if let Some(first) = gen_model.as_ref() {
                let model = registry.get_model(first.model_id());
                let modelcards = ModelIter::from_ffi(gen_model, registry);
                let res = || crate::load(registry, model, &*ckt, modelcards);
                err_handler(res, model, LOAD_ROUTINE_NAME)
            } else {
                0
            }
        },
        LOAD_ROUTINE_NAME,
    )
}

const_cstr!(SET_MODEL_PAR_NAME = "set_model_parameter");

/// Call from ngspice to set a model parameter
pub unsafe extern "C" fn set_model_parameter(
    id: ::std::os::raw::c_int,
    val: *mut IFvalue,
    model: *mut GENmodel,
) -> ::std::os::raw::c_int {
    ffi_wrapper(
        |registry| {
            let mut mc = ModelCard::from_ffi(model, registry);
            let param = ParameterId::from_raw(id.try_into().unwrap());
            let model = registry.get_model(mc.model_id());
            mc.set_param(param, &*val, model);
            0
        },
        SET_MODEL_PAR_NAME,
    )
}

const_cstr!(SETUP_NAME = "setup");

pub unsafe extern "C" fn setup(
    matrix: *mut SMPmatrix,
    gen_model: *mut GENmodel,
    ckt: *mut CKTcircuit,
    state_vec_offset: *mut c_int,
) -> c_int {
    ffi_wrapper(
        |regirsty| {
            if let Some(first) = gen_model.as_ref() {
                let model = regirsty.get_model(first.model_id());
                let modelcards = ModelIter::from_ffi(gen_model, regirsty);
                let res = || {
                    crate::setup(
                        regirsty,
                        model,
                        &mut *matrix,
                        &mut *ckt,
                        &mut *state_vec_offset,
                        modelcards,
                    )
                };
                err_handler(res, model, SETUP_NAME)
            } else {
                0
            }
        },
        SETUP_NAME,
    )
}

const_cstr!(UNSETUP_NAME = "unsetup");

pub unsafe extern "C" fn unsetup(gen_model: *mut GENmodel, ckt: *mut CKTcircuit) -> c_int {
    ffi_wrapper(
        |regirsty| {
            if let Some(first) = gen_model.as_ref() {
                let model = regirsty.get_model(first.model_id());
                let modelcards = ModelIter::from_ffi(gen_model, regirsty);
                let res = || crate::unsetup(regirsty, model, &mut *ckt, modelcards);
                err_handler(res, model, UNSETUP_NAME)
            } else {
                0
            }
        },
        UNSETUP_NAME,
    )
}
const_cstr!(CONV_TEST_NAME = "convTest");

pub unsafe extern "C" fn conv_test(gen_model: *mut GENmodel, ckt: *mut CKTcircuit) -> c_int {
    ffi_wrapper(
        |regirsty| {
            if let Some(first) = gen_model.as_ref() {
                let model = regirsty.get_model(first.model_id());
                let modelcards = ModelIter::from_ffi(gen_model, regirsty);
                let res = || crate::check_convergens(regirsty, model, &mut *ckt, modelcards);
                err_handler(res, model, CONV_TEST_NAME)
            } else {
                0
            }
        },
        CONV_TEST_NAME,
    )
}

pub fn ngspice_type(osdi_type: &TypeInfo) -> c_int {
    // cast is save here since all the flags are known at compile time to fit into an int
    ngspice_type_internal(osdi_type) as i32
}

fn ngspice_type_internal(osdi_type: &TypeInfo) -> u32 {
    let elem = match osdi_type.element {
        SimpleType::Real => IF_REAL,
        SimpleType::Integer => IF_INTEGER,
        SimpleType::String => IF_STRING,
        SimpleType::Bool => IF_FLAG,
    };
    if osdi_type.dimensions.is_empty() {
        elem
    } else {
        IF_VECTOR | elem
    }
}

pub fn ngspice_param_type(osdi_type: &UserInfo) -> c_int {
    let data_type = osdi_type.ty.with_info(|info| ngspice_type_internal(info));
    if osdi_type.obsolete {
        (data_type | IF_REDUNDANT | IF_SET) as i32
    } else {
        (data_type | IF_REDUNDANT) as i32
    }
}

impl CKTcircuit {
    pub fn make_node<'r>(
        &mut self,
        instance: IFuid,
        name: OsdiStr,
    ) -> Result<&'r mut CKTnode, NgspiceError> {
        let mut tmp: *mut CKTnode = std::ptr::null_mut();
        let err = unsafe { CKTmkVolt(self as *mut Self, &mut tmp, instance, name.cstr()) };
        Result::from_status_code(err).map(|_| unsafe { tmp.as_mut() }.unwrap())
    }

    pub fn delete_node(&mut self, idx: c_int) -> Result<(), NgspiceError> {
        let code = unsafe { CKTdltNNum(self as *mut Self, idx) };
        Result::from_status_code(code)
    }

    pub fn copy_nodesets(&mut self, instance: &GENinstance, terminal: c_int, dst: &mut CKTnode) {
        let mut terminal_node = std::ptr::null_mut();
        let mut _name = std::ptr::null_mut();
        if self.CKTcopyNodesets() != 0
            && unsafe {
                CKTinst2Node(
                    self as *mut Self,
                    instance as *const GENinstance as *mut GENinstance as *mut c_void,
                    terminal,
                    &mut terminal_node,
                    &mut _name,
                )
            } == 0
        {
            let terminal_node = &mut unsafe { *terminal_node };
            let nodeset_given = terminal_node.nsGiven();
            if nodeset_given != 0 {
                dst.nodeset = terminal_node.nodeset;
                dst.set_nsGiven(nodeset_given);
            }
        }
    }
}
