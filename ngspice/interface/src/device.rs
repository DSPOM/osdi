//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::ffi::{
    conv_test, load, set_model_parameter, setup, unsetup, IFdevice, IFparm, SPICEdev,
};
use crate::instance::OsdiInstanceData;
use crate::modelcard::ModelCardData;
use osdi::{ModelInfoStore, OsdiModel};
use std::convert::TryInto;
use std::os::raw::c_int;

impl SPICEdev {
    /// # Safety
    /// All strings are considered to be constants. Mutating them trough
    /// the pointer (even when passed to C) is UB
    pub unsafe fn from_osdi(model: &OsdiModel) -> Self {
        let instance_size = OsdiInstanceData::layout(model).size();
        let mc_size = ModelCardData::layout(model).size();
        Self {
            DEVpublic: IFdevice::from_info_store(&model.info_store),
            DEVparam: None,
            DEVmodParam: Some(set_model_parameter),
            DEVload: Some(load),
            DEVsetup: Some(setup),
            DEVunsetup: Some(unsetup),
            DEVpzSetup: None,
            DEVtemperature: None, // TODO prototype
            DEVtrunc: None,
            DEVfindBranch: None,
            DEVacLoad: None,
            DEVaccept: None,
            DEVdestroy: None,
            DEVmodDelete: None,
            DEVdelete: None,
            DEVsetic: None,
            DEVask: None,
            DEVmodAsk: None,
            DEVpzLoad: None,
            DEVconvTest: if model.info_store.non_linear_voltages.is_empty() {
                None
            } else {
                Some(conv_test)
            },
            DEVsenSetup: None,
            DEVsenLoad: None,
            DEVsenUpdate: None,
            DEVsenAcLoad: None,
            DEVsenPrint: None,
            DEVsenTrunc: None,
            DEVdisto: None,
            DEVnoise: None,
            DEVsoaCheck: None,
            DEVinstSize: Box::into_raw(Box::new(instance_size.try_into().unwrap())),
            DEVmodSize: Box::into_raw(Box::new(mc_size.try_into().unwrap())),
        }
    }
}

const ZERO_PTR: *mut c_int = &0 as *const c_int as *mut c_int;

impl IFdevice {
    unsafe fn from_info_store(info: &ModelInfoStore) -> Self {
        // we leak all info because its expected to be static by ngspice anyway

        let term_names: Box<[_]> = info
            .ports
            .iter()
            .map(|port| info.nodes[port.net].name.cstr())
            .collect();

        let term_names = Box::leak(term_names).as_mut_ptr();

        let model_params: Box<[_]> = info
            .parameters
            .iter_enumerated()
            .map(|(id, param)| IFparm::from_info_store(id, param))
            .collect();

        let model_param = Box::leak(model_params).as_mut_ptr();

        Self {
            name: info.name.cstr(),
            description: info.description.cstr(),
            terms: Box::into_raw(Box::new(info.ports.len().try_into().unwrap())),
            numNames: Box::into_raw(Box::new(info.ports.len().try_into().unwrap())),
            termNames: term_names,
            // TODO instance parameter
            numInstanceParms: ZERO_PTR,
            instanceParms: std::ptr::null_mut(),
            numModelParms: Box::into_raw(Box::new(info.ports.len().try_into().unwrap())),
            modelParms: model_param,
            flags: 0,
        }
    }
}
