//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use crate::errors::{OutOfBoundsParameters, SetupError};
use crate::ffi::{self, ngspice_param_type, CKTcircuit, GENmodel, IFparm, SMPmatrix};
use crate::instance::InstanceIter;
use crate::OSDI_DEVICE_OFFSET;
use osdi::model_info_store::{Parameter, ParameterId};
use osdi::runtime::{ModelId, ModelSizedType, OsdiRegistry};
use osdi::types::SimpleType;
use osdi::{OsdiModel, OsdiSlice};
use osdi_derive::osdi_model_data;
use std::convert::TryInto;
use std::ptr::NonNull;

impl IFparm {
    pub unsafe fn from_info_store(id: ParameterId, param: &Parameter) -> Self {
        Self {
            keyword: param.info.name.cstr(),
            id: id.raw().try_into().unwrap(),
            dataType: ngspice_param_type(&param.info),
            description: param.info.description.cstr(),
        }
    }
}

#[osdi_model_data]
pub struct ModelCard {
    /// NGSPICE has a general model struct.
    /// It is expected to be at the start of each model struct so that
    /// * mut GENmodel can be cast to *mut ModelCard (and back)
    pub general_model: GENmodel,

    #[model_data]
    pub parameters: osdi::runtime::abi::ModelCard,

    #[model_data]
    pub variables: osdi::runtime::abi::ModelVariables,
}

impl GENmodel {
    pub fn model_id(&self) -> ModelId {
        ModelId::new(self.GENmodType as usize - OSDI_DEVICE_OFFSET)
    }
}

unsafe impl ModelSizedType for ModelCardHead {
    fn model(&self) -> ModelId {
        self.general_model.model_id()
    }
}

impl ModelCard<'_> {
    pub unsafe fn from_ffi(gen: *mut GENmodel, registry: &OsdiRegistry) -> Self {
        ModelCard::from_ptr_with_registry(gen as *mut ModelCardData, registry)
    }

    pub unsafe fn instances<'a>(&self, regirstry: &'a OsdiRegistry) -> InstanceIter<'a> {
        InstanceIter::from_ffi(self.general_model.GENinstances, regirstry)
    }

    pub fn model_id(&self) -> ModelId {
        self.general_model.model_id()
    }

    pub fn setup(
        &mut self,
        registry: &OsdiRegistry,
        model: &OsdiModel,
        matrix: *mut SMPmatrix,
        ckt: &mut CKTcircuit,
        state_vec_offset: &mut ::std::os::raw::c_int,
    ) -> Result<(), SetupError> {
        // Setup default values and check that values are valid
        let out_of_bounds_paras = unsafe { self.parameters.setup(model) };

        if !out_of_bounds_paras.is_empty() {
            return Err(OutOfBoundsParameters {
                parameters: out_of_bounds_paras,
                model_name: self.general_model.GENmodName,
                model: model.id,
            }
            .into());
        }

        // TODO flags
        unsafe {
            self.variables.init(&self.parameters, model);
        }

        // setup all instances
        for mut instance in unsafe { self.instances(registry) } {
            instance.setup(model, self, matrix, ckt, state_vec_offset)?
        }

        Ok(())
    }

    pub fn set_param(&mut self, param: ParameterId, val: &ffi::IFvalue, model: &OsdiModel) {
        let ty = model.info_store.parameters[param].info.ty;
        ty.with_info(|ty| {
            let len = ty.size();
            match ty.element {
                SimpleType::Integer if len == 1 => {
                    self.parameters
                        .set_param(&model.info_store, param, unsafe { val.iValue })
                }
                SimpleType::Real if len == 1 => {
                    self.parameters
                        .set_param(&model.info_store, param, unsafe { val.rValue })
                }
                SimpleType::String if len == 1 => {
                    self.parameters
                        .set_param(&model.info_store, param, unsafe { val.sValue })
                }

                SimpleType::Bool if len == 1 => {
                    self.parameters
                        .set_param(&model.info_store, param, unsafe { val.iValue } != 0)
                }

                SimpleType::Integer => {
                    let data = unsafe { std::slice::from_raw_parts(val.v.vec.iVec, len as usize) };
                    self.parameters.set_param(
                        &model.info_store,
                        param,
                        OsdiSlice::new(data.to_vec()),
                    )
                }

                SimpleType::Real => {
                    let data = unsafe { std::slice::from_raw_parts(val.v.vec.rVec, len as usize) };
                    self.parameters.set_param(
                        &model.info_store,
                        param,
                        OsdiSlice::new(data.to_vec()),
                    )
                }

                SimpleType::String => {
                    let data = unsafe { std::slice::from_raw_parts(val.v.vec.sVec, len as usize) };
                    self.parameters.set_param(
                        &model.info_store,
                        param,
                        OsdiSlice::new(data.to_owned()),
                    )
                }

                SimpleType::Bool => {
                    let data = unsafe { std::slice::from_raw_parts(val.v.vec.iVec, len as usize) };
                    let data: Vec<bool> = data.iter().map(|&x| x != 0).collect();
                    self.parameters
                        .set_param(&model.info_store, param, OsdiSlice::new(data))
                }
            }
        });
    }
}

pub struct ModelIter<'a>(Option<NonNull<GENmodel>>, &'a OsdiRegistry);

impl<'a> ModelIter<'a> {
    pub unsafe fn from_ffi(ptr: *mut GENmodel, registry: &'a OsdiRegistry) -> Self {
        Self(NonNull::new(ptr), registry)
    }
}

impl<'a> Iterator for ModelIter<'a> {
    type Item = ModelCard<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        let raw = self.0?;
        let mc = unsafe { ModelCard::from_ffi(raw.as_ptr(), self.1) };
        self.0 = NonNull::new(mc.general_model.GENnextModel);
        Some(mc)
    }
}
