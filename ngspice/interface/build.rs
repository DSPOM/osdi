extern crate bindgen;

use std::env;
use std::path::{PathBuf};

fn main() {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=src/wrapper.h");

    let ngspice_src = PathBuf::from(std::env::var_os("NGSPICE_SRC").expect("NGSPICE_SRC must ne set to a directory containing the source of ngspice"));
    ngspice_src.canonicalize().expect("NGSPICE_SRC directory was not found!");
    let include_dir = ngspice_src.join("src").join("include");

    let bindings = bindgen::Builder::default()
        .clang_arg("--include-directory")
        .clang_arg(include_dir.to_str().unwrap())
        // Large types
        .whitelist_type("SPICEdev")
        .whitelist_type("CKTcircuit")
        // Analysis modes
        .whitelist_var("MODETRAN")
        .whitelist_var("MODEAC")
        // Load routine behavioural modes
        .whitelist_var("MODEINITFLOAT")
        .whitelist_var("MODEINITJCT")
        .whitelist_var("MODEINITFIX")
        .whitelist_var("MODEINITSMSIG")
        .whitelist_var("MODEINITTRAN")
        .whitelist_var("MODEINITPRED")
        // Parameter Type Flags
        .whitelist_var("IF_INTEGER")
        .whitelist_var("IF_REAL")
        .whitelist_var("IF_FLAG")
        .whitelist_var("IF_STRING")
        .whitelist_var("IF_REQUIRED")
        .whitelist_var("IF_VECTOR")
        .whitelist_var("IF_SET")
        .whitelist_var("IF_REDUNDANT")
        // Status codes
        .whitelist_var("E_NOMEM")
        .whitelist_var("E_PANIC")
        .whitelist_var("E_PARMVAL")
        .whitelist_var("E_PRIVATE")
        // Error locations
        .whitelist_var("errMsg")
        .whitelist_var("errRtn")
        // setup callbacks
        .whitelist_function("CKTmkVolt")
        .whitelist_function("CKTinst2Node")
        .whitelist_function("SMPmakeElt")
        // unsetup callbacks
        .whitelist_function("CKTdltNNum")
        .header("src/wrapper.h")
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("devdefs.rs"))
        .expect("Couldn't write bindings!");
}
