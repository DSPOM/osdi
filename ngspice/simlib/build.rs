extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=src/wrapper.h");


    let ngspice_src = PathBuf::from(std::env::var_os("NGSPICE_SRC").expect("NGSPICE_SRC must ne set to a directory containing the source of ngspice"));
    ngspice_src.canonicalize().expect("NGSPICE_SRC directory was not found!");
    let include_dir = ngspice_src.join("src").join("include");


    let bindings = bindgen::Builder::default()
        .clang_arg("--include-directory")
        .clang_arg(include_dir.to_str().unwrap())

        // Large types
        .whitelist_type("CKTcircuit")

        // Errors
        .whitelist_var("errMsg")
        .whitelist_var("errRtn")
        .whitelist_var("E_ORDER")
        // Integration methods
        .whitelist_var("GEAR")
        .whitelist_var("TRAPEZOIDAL")
        .header("src/wrapper.h")
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("ngspice.rs"))
        .expect("Couldn't write bindings!");
}
