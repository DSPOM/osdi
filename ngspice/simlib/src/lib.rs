//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use std::ffi::{c_void, CString};
use std::os::raw::{c_int, c_char};
use crate::ngspice::{CKTstates, CKTcircuit, TRAPEZOIDAL, GEAR, E_ORDER, DDTInputs};
use std::num::NonZeroI32;
use std::intrinsics::transmute;

mod lim;
mod ngspice;

macro_rules! const_cstr {
    ($content:literal) => {
        concat!($content, "\0").as_ptr() as *const c_char
    };

    ($name:ident = $content:literal) => {
        const $name: *const c_char = const_cstr!($content);
    };
}


const_cstr!(DDT_FUN_NAME = "ddt");

// macro_rules! ddt{
//     ($($n:literal),*) => {
//         $(
//             ::paste::paste!{
//                 #[no_mangle]
//                 pub unsafe extern "C" fn [< ddt_ $n >](ckt: *const c_void, state_vec: *mut *mut f64, offset: usize, derivatives: *mut f64) -> c_int {
//                     let ckt = &*(ckt as *const CKTcircuit);
//                     let state_vec = state_vec as *mut CKTstates;
//                     let state_vec = std::ptr::read(state_vec);
//                     let derivatives = derivatives as *mut [f64;$n];
//                     ddt_impl(ckt,state_vec,offset, &mut *derivatives)
//                 }
//             }
//
//
//         )*
//     }
// }

// Optimized performance by allowing loop to be unrolled by emulating const generics (const generics are used for the actual implementation but that doesn't work over ffi)
// OpenVAF will select the correct version based upon how many derivatives are required
//ddt!(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32);



macro_rules! gear {
    ($ckt: ident, $state_vec: ident, $($order: expr),+) => {
        0.0 $( + $ckt.CKTag[$order] * $state_vec[$order][0] )*
    };
}

pub unsafe extern "C" fn ddt(ckt: *const c_void, state_vec: *mut *mut f64, val: f64, ddt_index: u16, partial_derivative: *mut f64) -> c_int {

    // Calculate the posistion of data

    let offset = 2* (ddt_index as usize); //Store both input and output in the state vector
    let mut state_vec = std::ptr::read(state_vec as *mut CKTstates);
    for ptr in &mut state_vec{
        *ptr = ptr.add(offset)
    }
    let state_vec: DDTInputs = transmute(state_vec);

    // Write into state vec
    state_vec[0][0] = val;

    let ckt = &*(ckt as *const CKTcircuit);
    match ddt_impl(ckt, &state_vec){
        Ok((res,partial)) => {
            state_vec[0][1] = res;
            *partial_derivative = partial;
            0
        }
        Err(err) => {
            err.into()
        }
    }
}

fn ddt_impl(ckt: &CKTcircuit, state_vec: &DDTInputs) -> Result<(f64,f64),NonZeroI32> {
    let base =  ckt.CKTag[0] * state_vec[0][0];
    let delta = match (ckt.CKTintegrateMethod, ckt.CKTorder){
        (TRAPEZOIDAL,1) => { // Euler integration
            ckt.CKTag[1] * state_vec[1][0]
        },
        (TRAPEZOIDAL,2) => {
            -ckt.CKTag[0] * state_vec[1][0] - ckt.CKTag[1] * state_vec[1][1]
        },

        (GEAR,1) => {
             gear!(ckt, state_vec,  1)
        }

        (GEAR,2) => {
            gear!(ckt, state_vec,  1,2)
        }


        (GEAR,3) => {
            gear!(ckt, state_vec, 1,2,3)
        }


        (GEAR,4) => {
            gear!(ckt, state_vec,  1,2,3,4)
        }


        (GEAR,5) => {
            gear!(ckt, state_vec, 1,2,3,4,5)
        }


        (GEAR,6) => {
            gear!(ckt, state_vec, 1,2,3,4,5,6)
        }

        (TRAPEZOIDAL, order)|(GEAR, order) => {
            let err = format!("OSDI_SIMLIB: Illegal integration order: {}", order);
            if let Ok(msg) = CString::new(err){
                let msg = Box::leak(msg.into_boxed_c_str());
                unsafe{ngspice::errMsg=msg.as_ptr() as *mut c_char};
                unsafe{ngspice::errRtn=DDT_FUN_NAME as *mut c_char};
            }
            return Err(E_ORDER)
        }
        (method,_) => {
            let err = format!("OSDI_SIMLIB: Unknown integration method: {}! Valid valus are Gear ({}) and Trapezoidal ({})", method, GEAR, TRAPEZOIDAL);
            if let Ok(msg) = CString::new(err){
                let msg = Box::leak(msg.into_boxed_c_str());
                unsafe{ngspice::errMsg=msg.as_ptr() as *mut c_char};
                unsafe{ngspice::errRtn=DDT_FUN_NAME as *mut c_char};
            }
            return Err(E_ORDER)
        }
    };

    Ok((base+delta, delta))
}