//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


// TODO convergence checking directly in pnjlim?
//            if ( (!(ckt->CKTmode & MODEINITFIX))||(!(here->HICUMoff))) {
//                 if (icheck != 0) {
//                     ckt->CKTnoncon++;
//                     ckt->CKTtroubleElt = (GENinstance *) here;
//                 }
//             }

use std::os::raw::c_char;

/// Limit the per-iteration change of PN junction voltages
///
/// This code has been fixed by Alan Gillespie adding limiting for negative voltages.
/// Code has been adapted to Rust and OSDI by Pascal Kuthe (2020)
#[no_mangle]
pub extern "C" fn pnjlim(vnew: f64, vold: f64, converged: *mut c_char, vt: f64, vcrit: f64) -> f64 {
    if (vnew > vcrit) && ((vnew - vold).abs() > (vt + vt)) {
        unsafe { *converged = 0 }

        if vold > 0.0 {
            let arg = (vnew - vold) / vt;
            if arg > 0.0 {
                vold + vt * (2.0 + (arg - 2.0).ln())
            } else {
                vold - vt * (2.0 + (2.0 - arg).ln())
            }
        } else {
            vt * (vnew / vt).ln()
        }
    } else if vnew < 0.0 {
        let arg = if vold > 0.0 {
            -vold - 1.0
        } else {
            2.0 * vold - 1.0
        };

        if vnew < arg {
            unsafe { *converged = 0 }
            arg
        } else {
            vnew
        }
    } else {
        vnew
    }
}

#[no_mangle]
pub extern "C" fn limvds(vnew: f64, vold: f64, converged: *mut c_char) -> f64 {
    let res = if vold >= 3.5 {
        if vnew > vold {
            f64::min(vnew, (3.0 * vold) + 2.0)
        } else if vnew < 3.5 {
            f64::max(vnew, 2.0)
        } else {
            return vnew
        }

    } else if vnew > vold {
        f64::min(vnew, 4.0)
    } else {
        f64::max(vnew, -0.5)
    };

    if vnew != res{
        unsafe{*converged = 0}
    }
    res
}


/// Limit the per-iteration change of FET voltages
///
/// This code has been fixed by Alan Gillespie: A new definition for vtstlo.
/// Code has been adapted to Rust and OSDI by Pascal Kuthe (2020)
#[no_mangle]
pub extern "C" fn fetlim(vnew: f64, vold: f64, vto: f64, converged: *mut c_char) -> f64 {
    let vtsthi = 2.0 * (vold - vto).abs() + 2.0;
    let vtstlo = (vold - vto).abs() + 1.0;
    let vtox = vto + 3.5;
    let vdelta = vnew - vold;

    if vold >= vto {
        if vold >= vtox {
            if vdelta <= 0.0 {
                /* going off */
                if vnew >= vtox {
                    if -vdelta > vtstlo {
                        unsafe{*converged = 0}
                        vold - vtstlo
                    } else {
                        vnew
                    }
                } else {
                    let res = f64::max(vnew, vto + 2.0);
                    if res != vnew{
                        unsafe{*converged = 0 }
                    }
                    res
                }
            } else if vdelta >= vtsthi {
                vold + vtsthi
            } else {
                vnew
            }
        } else if vdelta <= 0.0 {
            /* decreasing */
            let res = f64::max(vnew, vto - 0.5);
            if res != vnew{
                unsafe{*converged = 0 }
            }
            res
        } else {
            /* increasing */
            let res = f64::max(vnew, vto + 4.0);
            if res != vnew{
                unsafe{*converged = 0 }
            }
            res
        }
    } else if vdelta <= 0.0 {
        if -vdelta > vtsthi {
            vold - vtsthi
        } else {
            vnew
        }
    } else {
        let vtemp = vto + 0.5;
        if vnew <= vtemp {
            if vdelta > vtstlo {
                unsafe{*converged = 0}
                vold + vtstlo
            } else {
                vnew
            }
        } else {
            unsafe{*converged = 0}
            vtemp
        }
    }
}