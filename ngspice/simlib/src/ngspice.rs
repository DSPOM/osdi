//  * ******************************************************************************************
//  *  Copyright (c) 2020 Pascal Kuthe. This file is part of the OSDI project.
//  *  It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/osdi/blob/master/LICENSE.
//  *  No part of OpenVAF-Types, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************


use std::os::raw::c_int;


#[allow(warnings)]
mod bindings{
    include!(concat!(env!("OUT_DIR"), "/ngspice.rs"));
}
pub type CKTstates = [*mut f64;8];
pub type DDTInputs<'a> = [&'a mut [f64; 2];8];
pub use bindings::*;
use std::num::NonZeroI32;
use static_assertions::{assert_eq_size,assert_eq_align};

pub const TRAPEZOIDAL: c_int = bindings::TRAPEZOIDAL as c_int;
pub const GEAR: c_int =  bindings::GEAR as c_int;
pub const E_ORDER: NonZeroI32 = unsafe { NonZeroI32::new_unchecked(bindings::E_ORDER as c_int) };



// This will fail to compile if ngspice changes the amount of timesteps saved

#[doc(hidden)]
unsafe fn _assert(){
    let _: CKTstates = (*(::std::ptr::null::<CKTcircuit>())).CKTstates;
}
assert_eq_size!(DDTInputs, CKTstates);
assert_eq_align!(DDTInputs, CKTstates);